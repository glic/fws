//
//  Utiity.m
//  frameworks
//
//  Created by ateliee on 2014/08/25.
//  Copyright (c) 2014年 minato. All rights reserved.
//
#import "Utility.h"

@implementation Utility

// 現在のウィンドウを取得
+(UIWindow*) getWindow{
    UIWindow *window = [UIApplication sharedApplication].keyWindow;
    if (!window) {
        window = [[UIApplication sharedApplication].windows objectAtIndex:0];
    }
    return window;
}
// 最上位のViewControllerを取得
-(UIViewController *)getSuperViewController:(UIViewController *)view{
    UIViewController *parentView = view;
    UIViewController *tmpView;
    while((tmpView = [parentView parentViewController])){
        parentView = tmpView;
    }
    return parentView;
}

// キャッシュファイルの削除
+(void) removeCache{
    // キャッシュ容量を０に変更
    [[NSURLCache sharedURLCache] setMemoryCapacity:0];
    // キャッシュ削除
    [[NSURLCache sharedURLCache] removeAllCachedResponses];
    // キャッシュ削除
    NSURLCache *sharedCache = [[NSURLCache alloc] initWithMemoryCapacity:0 diskCapacity:0 diskPath:nil];
    [NSURLCache setSharedURLCache:sharedCache];
    //[sharedCache release];
}

// 同期通信
+(NSString*) synchronousRequestURL:(NSString *)url Erorr:(NSError **)err Encoding:(int *)enc{
    // HTMLデータの同期通信
    NSURLResponse *res = nil;
    NSData *data = [NSURLConnection sendSynchronousRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:url] cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData timeoutInterval:10.0f] returningResponse:&res error:(err)];
    NSInteger statusCode = [((NSHTTPURLResponse *)res) statusCode];
    if(statusCode >= 400){
        (*err) = [[NSError alloc]init];
    }
    // 文字コード
    int encoding[] = {
        NSUTF8StringEncoding,         // UTF-8
        NSShiftJISStringEncoding,     // Shift-JIS
        NSJapaneseEUCStringEncoding,  // EUC-JP
        NSISO2022JPStringEncoding,    // JIS
        NSUnicodeStringEncoding,      // Unicode
        NSASCIIStringEncoding         // ASCII
    };
    // 取得したデータを文字列に変換
    NSString* string = nil;
    int max = sizeof(encoding) / sizeof(encoding[0]);
    for(int i=0;i<max;i++){
        string = [[NSString alloc] initWithData:data encoding:encoding[i]];
        if(string != nil){
            *enc = encoding[i];
            break;
        }
    }
    return string;
}

// 中心フレームを取得
+(CGRect) getCenterRectMake:(CGSize)size window:(CGSize)window{
    return CGRectMake(floor(((window).width - (size).width) / 2),floor(((window).height - (size).height) / 2),(size).width,(size).height);
}

@end

