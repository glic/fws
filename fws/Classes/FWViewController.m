//
//  FWViewController.m
//  fws
//
//  Created by ateliee on 2014/10/02.
//  Copyright (c) 2014年 minato. All rights reserved.
//

#import "FWViewController.h"
#import <objc/runtime.h>

@implementation FWViewController

- (ViewControllerNavigationDirection)_checkDirectionOnDisappear
{
    ViewControllerNavigationDirection dir = ViewControllerNavigationNone;
    NSArray *stack = self.navigationController.viewControllers;
    behindAnother = ([stack lastObject] != self);
    if (behindAnother) {
        if ([stack count] >= 2 && [stack objectAtIndex:([stack count] - 2)] == self) {
            dir = ViewControllerNavigationForward;
        } else {
            dir = ViewControllerNavigationBack;
        }
    }
    return dir;
}
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    ViewControllerNavigationDirection dir = ViewControllerNavigationNone;
    if (!alreadyViewed) {
        dir = ViewControllerNavigationForward;
    } else if (behindAnother) {
        dir = ViewControllerNavigationBack;
    }
    [self viewWillAppear:animated direction:dir];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    ViewControllerNavigationDirection dir = ViewControllerNavigationNone;
    if (!alreadyViewed) {
        dir = ViewControllerNavigationForward;
        alreadyViewed = YES;
    } else if (behindAnother) {
        dir = ViewControllerNavigationBack;
        behindAnother = NO;
    }
    [self viewDidAppear:animated direction:dir];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [self viewWillDisappear:animated direction:[self _checkDirectionOnDisappear]];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    [self viewDidDisappear:animated direction:[self _checkDirectionOnDisappear]];
}

-(void)viewWillAppear:(BOOL)animated direction:(ViewControllerNavigationDirection)direction{
}
-(void)viewDidAppear:(BOOL)animated direction:(ViewControllerNavigationDirection)direction{
    if (direction != ViewControllerNavigationBack) {
    }
}
-(void)viewWillDisappear:(BOOL)animated direction:(ViewControllerNavigationDirection)direction{
    
}
-(void)viewDidDisappear:(BOOL)animated direction:(ViewControllerNavigationDirection)direction{
    
}

@end
