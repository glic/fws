//
//  FWAlertView.m
//  fws
//
//  Created by ateliee on 2014/09/26.
//  Copyright (c) 2014年 minato. All rights reserved.
//

#import "FWAlertView.h"

@interface FWAlertView(){
    BOOL finished;
}
@end

@implementation FWAlertView

@synthesize clickedButtonAtIndex = clickedButtonAtIndex_;
@synthesize cancel = cancel_;
@synthesize alertView;

-(id) initWithTitle:(NSString*)title message:(NSString*)message cancelButtonTitle:(NSString*)cancelButtonTitle
          clickedBlock:(void (^)(UIAlertView* alertView, NSInteger buttonIndex))clickedButtonAtIndex otherButtonTitles:(NSString*)titles,...
{
    self = [self initWithClickedBlock:clickedButtonAtIndex];
    if (self) {
        self.alertView = [[UIAlertView alloc] initWithTitle:title message:message delegate:self cancelButtonTitle:cancelButtonTitle otherButtonTitles:titles, nil];
    }
    return self;
}

-(id)initWithClickedBlock:(void (^)(UIAlertView* alertView, NSInteger buttonIndex))clickedButtonAtIndex {
    self = [super init];
    if (self) {
        self.clickedButtonAtIndex = clickedButtonAtIndex;
        self.alertView = nil;
    }
    
    return self;
}
-(void) show{
    if (self.alertView) {
        CFRunLoopPerformBlock(CFRunLoopGetMain(), NSDefaultRunLoopMode, ^{
            [self.alertView show];
            
            finished = NO;
            while (!finished) {
                [[NSRunLoop currentRunLoop] runUntilDate:[NSDate dateWithTimeIntervalSinceNow:0.5f]];
            }
        });
    }
}

-(void)alertView:(UIAlertView *)view clickedButtonAtIndex:(NSInteger)buttonIndex{
    self.clickedButtonAtIndex(view, buttonIndex);
    finished = YES;
}

-(void)alertViewCancel:(UIAlertView *)view
{
    self.cancel(view);
    finished = YES;
}
@end