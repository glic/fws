//
//  define.h
//  frameworks
//
//  Created by ateliee on 2014/08/25.
//  Copyright (c) 2014年 minato. All rights reserved.
//
#import <CoreGraphics/CoreGraphics.h>
#import <Foundation/Foundation.h>

//---------
// 共通
//---------
// デバッグログ
#ifdef DEBUG
#define dbgLog(fmt,...) NSLog((@"%s [Line %d] " fmt),__PRETTY_FUNCTION__,__LINE__,##__VA_ARGS__)
#else
#define dbgLog(...)
#endif

#define APP ([UIApplication sharedApplication])
#define APP_DELEGATE ((AppDelegate *)[[UIApplication sharedApplication] delegate])
// OSバージョン
#define IOS_VERSION ([[[UIDevice currentDevice] systemVersion] floatValue])
// ビルドバージョン
#define APP_BUILD_VERSION ([[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleVersion"])
#define APP_DISPLAY_NAME ([[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleDisplayName"])
// Bundle参照
#define BUNDLE [NSBundle bundleForClass:[self class]]
// 画面サイズ
#define SCREEN_BOUNDS ([[UIScreen mainScreen] bounds])
#define SCREEN_SCALE ([UIScreen mainScreen].scale)
#define SCREEN_SCALE_RESIZE (1.0f / SCREEN_SCALE)
#define AFFINE_TRANSFORM_MAKE_SCREEN (CGAffineTransformMakeScale(SCREEN_SCALE_RESIZE,SCREEN_SCALE_RESIZE))
// デバイスサイズを取得
#define DEVICE_SCREEN_RECT (CGRectMake(0, 0, SCREEN_BOUNDS.size.width, SCREEN_BOUNDS.size.height))
// 各アイテムのサイズ
#define TABBAR_HEIGHT (49)
#define NAVIGATIONBAR_HEIGHT (44)
// UIColor
#define RGB(r,g,b) [UIColor colorWithRed:(r)/255.0 green:(g)/255.0 blue:(b)/255.0 alpha:1]
#define RGBA(r,g,b,a) [UIColor colorWithRed:(r)/255.0 green:(g)/255.0 blue:(b)/255.0 alpha:(a)]
// max,min
//#define MIN(a,b) ((a) < (b) ? (a) : (b))
//#define MAX(a,b) ((a) > (b) ? (a) : (b))
// NULLチェック(文字列)
#define STR_TO_INT(a,n) (((NSString *)(a) && ![((NSString *)(a)) isEqual:[NSNull null]]) ? ([((NSString *)(a)) intValue]) : (n))
#define STR_TO_FLOAT(a,n) (((NSString *)(a) && ![((NSString *)(a)) isEqual:[NSNull null]]) ? ([((NSString *)(a)) floatValue]) : (n))
#define STR_TO_DOUBLE(a,n) (((NSString *)(a) && ![((NSString *)(a)) isEqual:[NSNull null]]) ? ([((NSString *)(a)) doubleValue]) : (n))
// オブジェクト型に変換
#define OBJ_INT(a) ([NSNumber numberWithInt:(a)])
#define OBJ_FLOAT(a) ([NSNumber numberWithFloat:(a)])
#define OBJ_DOUBLE(a) ([NSNumber numberWithDouble:(a)])
#define OBJ_BOOL(a) ([NSNumber numberWithBool:(a)])
#define OBJ_CHAR(a) ([NSNumber numberWithChar:(a)])
// iPhone,iPadチェック
#define IS_IPHONE (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
#define IS_IPAD (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
// Retina判定
#define IS_RETINA ([[UIScreen mainScreen] respondsToSelector:@selector(scale)] && {UIScreen mainScreen].scale > 1)
// 言語取得
#define LANGUAGE ([NSLocale preferredLanguages][0])
// 画像取得
#define getImage(name) [UIImage imageNamed:name]
// リソースパスを取得
#define getResource(path) [[NSBundle mainBundle] pathForResource:[[path lastPathComponent] stringByDeletingPathExtension] ofType:[path pathExtension] inDirectory:[path stringByDeletingLastPathComponent]]
// URLエンコード・デコード
#define encodeURL(str) ((__bridge_transfer NSString*)CFURLCreateStringByReplacingPercentEscapesUsingEncoding(NULL, (CFStringRef)str, CFSTR(""), kCFStringEncodingUTF8))
#define decodeURL(str) ((__bridge NSString*)CFURLCreateStringByReplacingPercentEscapes(NULL, (CFStringRef)str,NULL,CFSTR("!*'();:@&=+$,/?%#[]"), kCFStringEncodingUTF8))
// フレームを取得
#define getFrameSize(view) (view.frame.size)
#define getFrameOrigin(view) (view.frame.origin)
// 配列キーチェック
#define IS_SET(arr,key) (([arr respondsToSelector:@selector(objectForKey:)]) && (([arr objectForKey:key]) != nil))
#define SAFE_GET(arr,key,default) (IS_SET(arr,key) ? arr[key] : default)
// ディレクトリ取得
// cacheディレクトリ(アプリがアクティブ時でも削除される可能性がある)
#define CACHE_DIRECTORY ([NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) objectAtIndex:0])
// tmpディレクトリ(アプリがアクティブなら保持される)
#define TMP_DIRECTORY (NSTemporaryDirectory())
// Documentsディレクトリ(アプリがアンインストールされない限り保持)
#define DOCUMENT_DIRECTORY ([NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0])

// CGRect,CGPoint,CGSize
static inline CGRect CGRectMove(CGRect rect, CGFloat dx,CGFloat dy){
    rect.origin.x = dx;
    rect.origin.y = dy;
    return rect;
}
static inline CGRect CGRectResize(CGRect rect, CGFloat width,CGFloat height){
    rect.size.width = width;
    rect.size.height = height;
    return rect;
}
static inline CGRect CGRectFrameCenter(CGSize r,CGSize frame){
    return CGRectMake(((frame.width - r.width) / 2), ((frame.height - r.height) / 2), r.width, r.height);
}
/*
// FWBox
struct FWBox {
    CGFloat top;
    CGFloat left;
    CGFloat right;
    CGFloat bottom;
};
typedef struct FWBox FWBox;

extern const FWBox FWBoxZero;

inline FWBox
FWBoxMake4(CGFloat top, CGFloat right, CGFloat bottom, CGFloat left)
{
    FWBox box;
    box.top = top; box.right = right;
    box.bottom = bottom; box.left = left;
    return box;
}
inline FWBox
FWBoxMake3(CGFloat top, CGFloat horizontal, CGFloat bottom)
{
    return FWBoxMake4(top, horizontal, bottom, horizontal);
}
inline FWBox
FWBoxMake2(CGFloat vertical, CGFloat horizontal)
{
    return FWBoxMake4(vertical, horizontal, vertical, horizontal);
}
inline FWBox
FWBoxMake1(CGFloat p)
{
    return FWBoxMake4(p, p, p, p);
}
inline FWBox
FWBoxMakeString(NSString* str)
{
    NSArray* a = [[str stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet] ] componentsSeparatedByString:@" "];
    if ([a count] == 1) {
        return FWBoxMake1([[a objectAtIndex:0] floatValue]);
    }else if ([a count] == 2) {
        return FWBoxMake2([[a objectAtIndex:0] floatValue],[[a objectAtIndex:1] floatValue]);
    }else if ([a count] == 3) {
        return FWBoxMake3([[a objectAtIndex:0] floatValue],[[a objectAtIndex:1] floatValue],[[a objectAtIndex:2] floatValue]);
    }else if ([a count] == 4) {
        return FWBoxMake4([[a objectAtIndex:0] floatValue],[[a objectAtIndex:1] floatValue],[[a objectAtIndex:2] floatValue],[[a objectAtIndex:3] floatValue]);
    }
    return FWBoxZero;
}*/

#if !defined(FW_EXTERN)
#  if defined(__cplusplus)
#   define FW_EXTERN extern "C"
#  else
#   define FW_EXTERN extern
#  endif
#endif /* !defined(CG_EXTERN) */

/* Definition of `CG_INLINE'. */

#if !defined(FW_INLINE)
# if defined(__STDC_VERSION__) && __STDC_VERSION__ >= 199901L
#  define FW_INLINE static inline
# elif defined(__cplusplus)
#  define FW_INLINE static inline
# elif defined(__GNUC__)
#  define FW_INLINE static __inline__
# else
#  define FW_INLINE static
# endif
#endif

/* Definition of `__CG_NO_INLINE'. */

// FWBox
struct FWBox {
    CGFloat top;
    CGFloat left;
    CGFloat right;
    CGFloat bottom;
};
typedef struct FWBox FWBox;

FW_EXTERN const FWBox FWBoxZero;

FW_INLINE FWBox
FWBoxMake4(CGFloat top, CGFloat right, CGFloat bottom, CGFloat left)
{
    FWBox box;
    box.top = top; box.right = right;
    box.bottom = bottom; box.left = left;
    return box;
}
FW_INLINE FWBox
FWBoxMake3(CGFloat top, CGFloat horizontal, CGFloat bottom)
{
    return FWBoxMake4(top, horizontal, bottom, horizontal);
}
FW_INLINE FWBox
FWBoxMake2(CGFloat vertical, CGFloat horizontal)
{
    return FWBoxMake4(vertical, horizontal, vertical, horizontal);
}
FW_INLINE FWBox
FWBoxMake1(CGFloat p)
{
    return FWBoxMake4(p, p, p, p);
}
FW_INLINE FWBox
FWBoxMakeString(NSString* str)
{
    NSArray* a = [[str stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet] ] componentsSeparatedByString:@" "];
    if ([a count] == 1) {
        return FWBoxMake1([[a objectAtIndex:0] floatValue]);
    }else if ([a count] == 2) {
        return FWBoxMake2([[a objectAtIndex:0] floatValue],[[a objectAtIndex:1] floatValue]);
    }else if ([a count] == 3) {
        return FWBoxMake3([[a objectAtIndex:0] floatValue],[[a objectAtIndex:1] floatValue],[[a objectAtIndex:2] floatValue]);
    }else if ([a count] == 4) {
        return FWBoxMake4([[a objectAtIndex:0] floatValue],[[a objectAtIndex:1] floatValue],[[a objectAtIndex:2] floatValue],[[a objectAtIndex:3] floatValue]);
    }
    FWBox b;
    b.top = b.bottom = b.left = b.right = 0.0f;
    return b;
}

#if __IPHONE_OS_VERSION_MAX_ALLOWED < 60000
@interface NSDictionary(subscripts)
- (id)objectForKeyedSubscript:(id)key;
@end

@interface NSMutableDictionary(subscripts)
- (void)setObject:(id)obj forKeyedSubscript:(id <NSCopying>)key;
@end

@interface NSArray(subscripts)
- (id)objectAtIndexedSubscript:(NSUInteger)idx;
@end

@interface NSMutableArray(subscripts)
- (void)setObject:(id)obj atIndexedSubscript:(NSUInteger)idx;
@end
#endif


