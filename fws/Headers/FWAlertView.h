//
//  FWAlertView.h
//  fws
//
//  Created by ateliee on 2014/09/26.
//  Copyright (c) 2014年 minato. All rights reserved.
//

#ifndef fws_alertView_h
#define fws_alertView_h

#import <UIKit/UIKit.h>

@interface FWAlertView : NSObject<UIAlertViewDelegate>{
    void (^clickedButtonAtIndex_)(UIAlertView* alertView, NSInteger buttonIndex);
    UIAlertView* alertView;
}

@property(nonatomic,copy) void (^clickedButtonAtIndex)(UIAlertView* alertView, NSInteger buttonIndex);
@property(nonatomic,copy) void (^cancel)(UIAlertView* alertView);
@property(nonatomic) UIAlertView* alertView;

-(id) initWithTitle:(NSString*)title message:(NSString*)message cancelButtonTitle:(NSString*)cancelButtonTitle
          clickedBlock:(void (^)(UIAlertView* alertView, NSInteger buttonIndex))clickedButtonAtIndex otherButtonTitles:(NSString*)titles,...;
-(id)initWithClickedBlock:(void (^)(UIAlertView* alertView, NSInteger buttonIndex))clickedButtonAtIndex ;
-(void)show;

@end

#endif
