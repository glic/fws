//
//  Utility.h
//  frameworks
//
//  Created by ateliee on 2014/08/25.
//  Copyright (c) 2014年 minato. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface Utility : NSObject

// 現在のウィンドウを取得
+(UIWindow*) getWindow;
// キャッシュファイルの削除
+(void) removeCache;
// 同期通信
+(NSString*) synchronousRequestURL:(NSString *)url Erorr:(NSError **)err Encoding:(int *)enc;
// 中心フレームを取得
+(CGRect) getCenterRectMake:(CGSize)size window:(CGSize)window;

@end
