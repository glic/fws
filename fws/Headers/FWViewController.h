//
//  FWViewController.h
//  fws
//
//  Created by ateliee on 2015/01/11.
//  Copyright (c) 2015年 minato. All rights reserved.
//

#ifndef fws_FWViewController_h
#define fws_FWViewController_h

#import <UIKit/UIKit.h>

typedef enum NSinteger{
    ViewControllerNavigationNone,
    ViewControllerNavigationForward,
    ViewControllerNavigationBack
}ViewControllerNavigationDirection;

@interface FWViewController : UIViewController{
    bool behindAnother;
    bool alreadyViewed;
}

- (ViewControllerNavigationDirection)_checkDirectionOnDisappear;

-(void)viewWillAppear:(BOOL)animated direction:(ViewControllerNavigationDirection)direction;
-(void)viewDidAppear:(BOOL)animated direction:(ViewControllerNavigationDirection)direction;
-(void)viewWillDisappear:(BOOL)animated direction:(ViewControllerNavigationDirection)direction;
-(void)viewDidDisappear:(BOOL)animated direction:(ViewControllerNavigationDirection)direction;
@end
#endif
