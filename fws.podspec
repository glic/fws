Pod::Spec.new do |s|
  s.name         = 'fws'
  s.version      = '1.0.1'
  s.summary      = 'Utility Classes.'
  s.description  = <<-DESC
                   Utility Classes files.
                   DESC

  s.authors      = {'ateliee' => 'info@glic.co.jp'}
  s.homepage     = 'http://glic.co.jp'
  s.license      = { :type => 'License, Version 1.0', :text => <<-LICENSE
    Licensed under the License, Version 1.0 (the "License");
    you may not use this file except in compliance with the License.

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
    LICENSE
  }
  s.platform     = :ios, '7.0'
  s.source       = { :git => 'https://ateliee1225@bitbucket.org/glic/fws.git', :tag => '1.0.1' }
  s.source_files  = 'fws/Classes', 'fws/Classes/*.{h,m}', 'fws/Headers/*.{h,m}'
  s.requires_arc  = true
  s.dependencies = { 'IOSHelper' => '>= 1.0.1','FWWebView' => '>= 1.0.1','AppDelegateNotification' => '>= 1.0.0' } 
end
