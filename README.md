# FWS
iosアプリ開発でよく利用するクラスをまとめています。

## エラーチェック
```
pod spec lint MyLib.podspec
```
記述に間違いないと
```
MyLib passed validation.
```
と表示されます。

## 更新方法
```
pod trunk push fws.podspec
```